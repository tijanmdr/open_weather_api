## Open Weather API 
- Vue JS 
- Axios 
- Open Weather API (https://openweathermap.org/)

### Webpack Installation
-  For webpack `npm install webpack -g`
-  For webpack development mode `npm install webpack-dev-server -g`

## Webpack Builds
- `webpack --watch` for file watcher
- `webpack-dev-server` for development mode