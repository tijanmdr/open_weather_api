new Vue({
    el: '#app',
    data: {
        cities: {},
        coords: {lat:0,lng:0},
        uri: 'http://api.openweathermap.org/data/2.5/weather?appid=4d692a449e7f8ee7aae962f8f440c837&',
        forecastURI: 'http://api.openweathermap.org/data/2.5/forecast?appid=4d692a449e7f8ee7aae962f8f440c837&',
        currentWeather: {}
    },
    mounted() {
        this.getWeather(this.coords);
        if ('geolocation' in navigator) {
            navigator.geolocation.getCurrentPosition(
                function(position) {
                    this.coords.lat = position.coords.latitude
                    this.coords.lng = position.coords.longitude
                    if (this.coords.lat !== 0 && this.coords.lng !== 0)
                        this.getWeather()
                }.bind(this),
                function (error) {
                    console.log('Error in locating')
                }
            ) // bind to `this` so it's the current component.
        }
        // this.getAllCities()
        setTimeout(()=>{
            this.initAutocomplete()
        }, 1000)
    },
    methods: {
        initAutocomplete() {
            let autocomplete = new google.maps.places.Autocomplete(
                (document.getElementById('citySelect')),
                {types: ['(cities)']}
            );
            autocomplete.addListener('place_changed', function () {
                var place = autocomplete.getPlace()
                console.log(place)
                this.coords = {lat: place.geometry.location.lat(),lng: place.geometry.location.lng()}
                if (this.coords.lat !== 0 && this.coords.lng !== 0)
                    this.getWeather()
            }.bind(this));
        },
        getWeather () {
            let degreeCelcius = [], labels = []
            axios.get(this.uri+'lat='+this.coords.lat+'&lon='+this.coords.lng)
                .then(res1=>{
                    this.currentWeather = res1.data
                })
            axios.get(this.forecastURI+'lat='+this.coords.lat+'&lon='+this.coords.lng)
                .then(res2=>{
                    // console.log(res2.data.list)
                    res2.data.list.forEach((data,key)=>{
                        console.log(data.dt_txt, (data.main.temp)-273.15)
                        if (key < 20) {
                            degreeCelcius.push(data.main.temp-273.15)
                            labels.push(data.dt_txt)
                        }
                        this.getHourChart(degreeCelcius, labels)
                    })
                })
        },
        getHourChart (data, labels) {
            var ctx = document.getElementById('hourChart').getContext('2d');
            var chart = new Chart(ctx, {
                // The type of chart we want to create
                type: 'bar',

                // The data for our dataset
                data: {
                    labels: labels,
                    datasets: [{
                        backgroundColor: 'rgb(255, 99, 132)',
                        borderColor: 'rgb(255, 99, 132)',
                        data: data,
                    }]
                },

                // Configuration options go here
                options: {}
            });
        }
    }
})